from PIL import Image, ImageOps, ImageEnhance
from google.cloud import vision

import Assets
import GoogleDetector
import io




class ImageEditor:
    faceDetector = GoogleDetector.FaceDetector()

    def deepfry(img):
        img = img.convert('RGB')
        width, height = img.width, img.height
        img = img.resize((int(width ** .75), int(height ** .85)), resample=Image.LANCZOS)
        img = img.resize((int(width ** .88), int(height ** .95)), resample=Image.BILINEAR)
        img = img.resize((int(width ** .9), int(height ** .99)), resample=Image.BICUBIC)
        img = img.resize((width, height), resample=Image.BICUBIC)
        img = ImageOps.posterize(img, 4)

        return img


    def resize_flare(Flare, face):
        width_factor = 4.5
        height_factor = 2.2
        face_w = face.width
        face_h = face.height
        return Flare.resize((int(face_w * width_factor), int(face_h * height_factor)))

    sample = Image.open(Assets.SAMPLE)
    flare = Assets.FLARE
    file = Assets.FILE

    def flare(self, image, output):
        face = GoogleDetector.detect_face(image)

        flare = self.resize_flare(self.flare, face)

        flare_center_x = int(flare.size[0] / 2)
        flare_center_y = int(flare.size[1] / 2)

        lEyeP = (face.left_eye[0] - flare_center_x, face.left_eye[1] - flare_center_y)
        rEyeP = (face.right_eye[0] - flare_center_x, face.right_eye[1] - flare_center_y)

        angle = 270 - (rEyeP[0] - lEyeP[0]) / (rEyeP[1] - lEyeP[1])
        flare = flare.rotate(angle)
        sample = Image.open(image)
        sample.paste(flare, lEyeP, flare)
        sample.paste(flare, rEyeP, flare)
        sample = self.deepfry(sample)

        sample.save(output, "JPEG")

    face = faceDetector.detect_face(Assets.SAMPLE)

    flare = resize_flare(flare, face)

    flareCenterX = int(flare.size[0] / 2)
    flareCenterY = int(flare.size[1] / 2)

    lEyeP = (face.left_eye[0] - flareCenterX, face.left_eye[1] - flareCenterY)
    rEyeP = (face.right_eye[0] - flareCenterX, face.right_eye[1] - flareCenterY)

    angle = 270 - (rEyeP[0] - lEyeP[0]) / (rEyeP[1] - lEyeP[1])
    flare = flare.rotate(angle)

    sample.paste(flare, lEyeP, flare)
    sample.paste(flare, rEyeP, flare)

    sample = deepfry(sample)

    sample.save("edited.jpeg", "JPEG")
