import io

from google.cloud import vision
import os


class FaceDetector:

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'C:/Users/Manu/Downloads/Lens Flare Bot-2d53a3546c16.json'
    
    client = vision.ImageAnnotatorClient()

    def detect_face(self, route):
        file = io.open(route, 'rb')
        content = file.read()
        image = vision.types.Image(content=content)
        faces = self.client.face_detection(image=image, max_results=1).face_annotations
        for landmark in faces[0].landmarks:
            if landmark.type == 1:
                l_eye_p = (int(landmark.position.x), int(landmark.position.y))

            if landmark.type == 2:
                r_eye_p = (int(landmark.position.x), int(landmark.position.y))

        return FaceContainer(
            int(faces[0].bounding_poly.vertices[1].x - faces[0].bounding_poly.vertices[0].x),
            int(faces[0].bounding_poly.vertices[2].y - faces[0].bounding_poly.vertices[1].y),
            l_eye_p,
            r_eye_p
        )


class FaceContainer:

    def __init__(self, width, height, right_eye, left_eye):
        self.width = width
        self.height = height
        self.right_eye = right_eye
        self.left_eye = left_eye
